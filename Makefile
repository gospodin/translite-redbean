all: build
redbean.com:
	wget https://redbean.dev/redbean-2.2.com -O redbean.com
	chmod +x redbean.com
zip.com:
	wget https://redbean.dev/zip.com -O zip.com
	chmod +x zip.com
brotli.com:
	wget https://cosmo.zip/pub/cosmos/bin/brotli -O brotli.com
	chmod +x brotli.com
build:	redbean.com zip.com brotli.com
	cd srv; ../zip.com ../redbean.com * .init.lua .lua/* css/*;
	cd .git/refs/heads; cp master .id; ../../../zip.com ../../../redbean.com .id; rm .id;
clean:
	rm redbean.com zip.com brotli.com
