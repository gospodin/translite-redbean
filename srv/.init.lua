engines = {
  google = require 'google',
  deepl = require 'deepl'
}
settings = require 'settings'
html = require 'html'
api = require 'api'
function OnHttpRequest()
  engine = engines[GetParam('engine') or 'google'] or engines.google
  text = GetParam('text') or ''
  selectedSl = GetParam('sl') or 'auto'
  selectedSl = engine.sl[selectedSl] and selectedSl or 'auto'
  selectedTl = GetParam('tl') or 'en'
  selectedTl = engine.tl[selectedTl] and selectedTl or 'en'
  local path = GetPath()
  if string.sub(path, 1, 4) == '/api' then
    RoutePath('/api.lua')
  elseif path == '/settings' then
    RoutePath('/settings.lua')
  else
    Route()
  end
end
-- special script called by main redbean process at startup
HidePath('/usr/share/zoneinfo/')
HidePath('/usr/share/ssl/')
