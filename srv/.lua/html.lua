local html = {}

function html.LanguageOptions(languageTable, selected)
  local selector = {}
  for code, value in pairs(languageTable) do
    local selected = (code == selected) and "selected" or ''
    table.insert(selector, [[<option %s value="%s">%s</option>]] % {selected, EscapeHtml(code), EscapeHtml(value)})
  end
  return table.concat(selector)
end
function html.EngineButtons(selected)
  local selector = {}
  for code, value in pairs(engines) do
    local selected = (code == selected) and [[style="text-decoration:underline"]] or ""
    table.insert(selector, [[<button %s type=submit name=engine value="%s">%s</button>]] % {selected, code, value.name})
  end
  return table.concat(selector)
end
return html
