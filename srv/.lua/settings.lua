local settings = {}
function settings.set(name, value)
  SetCookie(name, value, {
    Expires = GetTime() + (86400 * 90),
    Path = '/'
  })
end
function settings.reset(name)
  SetCookie(name, '', {
    Expires = 1,
    Path = '/'
  })
end
function settings.get(name)
  return GetCookie(name)
end
return settings
