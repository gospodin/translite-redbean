unix = require 'unix'
brotli = require 'brotli'
local deepl = {}

deepl.code = "deepl"
deepl.name = "DeepL"

deepl.tl = {
  ["bg"] = "Bulgarian",
  ["zh"] = "Chinese",
  ["cs"] = "Czech",
  ["da"] = "Danish",
  ["nl"] = "Dutch",
  ["en"] = "English",
  ["et"] = "Estonian",
  ["fi"] = "Finnish",
  ["fr"] = "French",
  ["de"] = "German",
  ["el"] = "Greek",
  ["hu"] = "Hungarian",
  ["it"] = "Italion",
  ["ja"] = "Japanese",
  ["lv"] = "Latvian",
  ["lt"] = "Lithuanian",
  ["pl"] = "Polish",
  ["pt"] = "Portugese",
  ["ro"] = "Romanian",
  ["ru"] = "Russian",
  ["sk"] = "Slovak",
  ["sl"] = "Slovenian",
  ["es"] = "Spanish",
  ["sv"] = "Swedish",
}

deepl.sl = {}
deepl.sl["auto"] = "Autodetect"
--copy by value
for k, v in pairs(deepl.tl) do
  deepl.sl[k] = v
end

deepl.id = ((Lemur64() % 99999) + 8300000) * 1000

local function GetFakeTimestamp(text)
  local timestamp = unix.clock_gettime()
  local iCount = 0

  for i in string.gmatch(text, 'i') do
    iCount = iCount + 1
  end

  if iCount ~= 0 then
    iCount = iCount + 1
    return timestamp - (timestamp % iCount ) + iCount
  else
    return timestamp
  end
end

local function GetParameter(text, sl, tl)
  deepl.id = deepl.id + 1
  local fakeTimestamp = GetFakeTimestamp(text)

  local parameter = '{"jsonrpc":"2.0","method":"LMT_handle_texts","id":%s,"params":{"texts":[{"text":"%s","requestAlternatives":1}],"splitting":"newlines","lang":{"source_lang_user_selected":"%s","target_lang":"%s"},"timestamp":%s,"commonJobParams":{"wasSpoken":false,"transcribe_as":""}}}' % {deepl.id, EscapeLiteral(text), sl, tl, fakeTimestamp}

  local replacement = ''
  if (deepl.id + 3) % 13 == 0 or (deepl.id + 5) % 29 == 0 then
    replacement = '"method" : "'
  else
    replacement = '"method": "'
  end

  return string.gsub(parameter, '"method":"', replacement)
end

function deepl.GetTranslation(text, sl, tl)
  assert(deepl.sl[sl], 'Invalid source language')
  assert(deepl.tl[tl], 'Invalid target language')
  if text == '' then return '' end
  local parameter = GetParameter(text, sl, tl)
  local headers = {
    ["Content-Type"] = "application/json",
    ["Accept"] = "*/*",
    ["x-app-os-name"] = "iOS",
    ["x-app-os-version"] = "16.3.0",
    ["Accept-Language"] = "en-US,en;q=0.9",
    ["Accept-Encoding"] = "gzip, deflate, br",
    ["x-app-device"] = "iPhone13,2",
    ["User-Agent"] = "DeepL-iOS/2.9.1 iOS 16.3.0 (iPhone13,2)",
    ["x-app-build"] = "510265",
    ["x-app-version"] = "2.9.1",
    ["Connection"] = "keep-alive",
  }
  local apiURL = "https://www2.deepl.com/jsonrpc";
  _, _, result =  Fetch(apiURL, {
    method = "POST",
    body = parameter,
    headers = headers
  })
  result = DecodeJson(brotli.decompress(result)).result.texts[1].text
  return result
end
return deepl
