re = require 're'
local google = {}
google.code = "google"
google.name = "Google"
local reGoogleResult = re.compile([[<div class="result-container">(.*)</div><div class="links-container">]])
local _, _, slBody = Fetch('https://translate.google.com/m?mui=sl')
local _, _, tlBody = Fetch('https://translate.google.com/m?mui=tl')

google.sl = {}
for code, value in slBody:gmatch([["%./m%?sl=([^&]*)&amp;tl&amp;hl=en%-US">([^<]*)]]) do
  google.sl[code] = value
end

google.tl = {}
for code, value in tlBody:gmatch([["%./m%?sl&amp;tl=([^&]*)&amp;hl=en%-US">([^<]*)]]) do
  google.tl[code] = value
end

function google.GetTranslation(text, sl, tl)
  assert(google.sl[sl], 'Invalid source language')
  assert(google.tl[tl], 'Invalid target language')
  if text == '' then return '' end
  local _, _, body = Fetch('https://translate.google.com/m' .. '?sl=' .. EscapeParam(sl) .. '&tl=' .. EscapeParam(tl) .. '&q=' .. EscapeParam(text))
  _, result = reGoogleResult:search(body)
  return result
end

return google
