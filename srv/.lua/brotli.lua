local unix = require 'unix'
local brotliExecutable
if path.isfile"./brotli.com" then
  brotliExecutable = "./brotli.com"
else
  brotliExecutable = assert(unix.commandv('brotli'))
end
local function BrotliDecompress(compressed)
  local result = {}
  fd1 = {assert(unix.pipe())}
  fd2 = {assert(unix.pipe())}
  -- fd1 - write to brotli
  -- fd2 - read from brotli
  if assert(unix.fork()) == 0 then
    assert(unix.close(fd1[2]))
    assert(unix.close(fd2[1]))
    assert(unix.dup(fd1[1], 0))
    assert(unix.close(fd1[1]))
    assert(unix.dup(fd2[2], 1))
    assert(unix.close(fd2[2]))
    unix.execve(brotliExecutable, {brotliExecutable, '-df'})
    unix.exit(127)
  else
    assert(unix.close(fd1[1]))
    assert(unix.close(fd2[2]))
    assert(unix.write(fd1[2], compressed))
    assert(unix.close(fd1[2]))
    while true do
      data, err = assert(unix.read(fd2[1]))
      if data then
        if data ~= '' then
          table.insert(result, data)
        else
          break
        end
      elseif err:errno() ~= EINTR then
        Log(kLogWarn, tostring(err))
        break
      end
    end
    assert(unix.close(fd2[1]))
    assert(unix.wait())
  end
  return table.concat(result)
end
return {decompress = BrotliDecompress}
