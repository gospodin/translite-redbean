re = require 're'
api = {}
local reApiMethod = re.compile("/api/([[:alnum:]_]*)")
function api.GetMethod(path)
  local _, method = reApiMethod:search(path)
  return method or 'help'
end
return api
