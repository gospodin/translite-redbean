if HasParam('save') then
  SetStatus(302)
  SetHeader('Location', './settings')
  if HasParam('js') and GetParam('js') == 'on' then
    settings.set('js', 'on')
  else
    settings.reset('js')
  end
  goto save
end

local checked = ''
if settings.get('js') == 'on' then
  checked = ' checked'
end

Write([[
<form>
  <label for="js">Enable JavaScript?</label>
  <input type=checkbox]]..checked..[[ id=js name=js />
  <button type=submit name="save">Save settings</button>
</form>
<a href="/">Go back</a>
]])
::save::
