local result = engine.GetTranslation(text, selectedSl, selectedTl)
local jsEnabled = settings.get('js') == 'on'

Write([[
<form>
  %s<br>
  <select id="sl" name="sl">%s</select><select id="tl" name="tl">"%s</select><br>
  <textarea name=text>%s</textarea>
  <textarea readonly>%s</textarea>
  <br>
  <button type=submit name=engine value="%s">Translate</button>
</form>
<a href="settings">Settings</a>
<a href="api">API</a>
]] % {
  html.EngineButtons(engine.code),
  html.LanguageOptions(engine.sl, selectedSl),
  html.LanguageOptions(engine.tl, selectedTl),
  EscapeHtml(text),
  EscapeHtml(result),
  engine.code
})

if jsEnabled then Write[[<script src="fluoride.js"></script>]] end
