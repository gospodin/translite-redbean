local function printHelp()
  Write[[
  /api/help - get this help<br>
  /api/translate?engine=ENGINE&sl=SL&tl=TL&text=TEXT - translate TEXT from SL to TL using ENGINE<br>
  /api/get_languages?engine=ENGINE - get available languages for ENGINE<br>
  /api/get_engines - get available engines
  ]]
end
local function printTranslation()
  Write(engine.GetTranslation(text, selectedSl, selectedTl))
end
local function printLanguages()
  local languages = {sl = {}, tl = {}}

  for code, value in pairs(engine.sl) do
    table.insert(languages.sl,{
      ["code"] = code,
      ["value"] = value
    })
  end
  for code, value in pairs(engine.tl) do
    table.insert(languages.tl,{
      ["code"] = code,
      ["value"] = value
    })
  end

  SetHeader('Content-Type', 'application/json')
  Write(EncodeJson(languages))
end
local function printEngines()
  local enginesList = {}
  for code, value in pairs(engines) do
    table.insert(enginesList, {
      ["code"] = code,
      ["name"] = value.name
    })
  end
  SetHeader('Content-Type', 'application/json')
  Write(EncodeJson(enginesList))
end

local method = api.GetMethod(GetPath())

local methods = {
  help = printHelp,
  translate = printTranslation,
  get_languages = printLanguages,
  get_engines = printEngines
}

if methods[method] then
  methods[method]()
else
  methods.help()
end
