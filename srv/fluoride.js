const slSelect = document.getElementById("sl")
const tlSelect = document.getElementById("tl")
const slValues = Array.from(slSelect.options).map(x => x.getAttribute('value'))
const tlValues = Array.from(tlSelect.options).map(x => x.getAttribute('value'))

const swapButton = document.createElement('button')
swapButton.innerHTML = '<->'
slSelect.insertAdjacentElement('afterend', swapButton)
swapButton.addEventListener("click", (event) => {
  event.preventDefault();
  if (slValues.includes(tlSelect.value) && tlValues.includes(slSelect.value)) {
    const temp = slSelect.value
    slSelect.value = tlSelect.value
    tlSelect.value = temp
  } else {
    alert("can't swap these languages")
  }
})
